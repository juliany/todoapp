# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.17-MariaDB)
# Database: todo_test
# Generation Time: 2017-02-21 12:35:38 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table to_do
# ------------------------------------------------------------

LOCK TABLES `to_do` WRITE;
/*!40000 ALTER TABLE `to_do` DISABLE KEYS */;

INSERT INTO `to_do` (`id`, `title`, `category`, `description`, `due_date`, `create_date`, `status`)
VALUES
	(5,'Task1','Symfony','Symfony\'s security system is incredibly powerful, but it can also be confusing to set up. In this article, you\'ll learn how to set up your application\'s security step-by-step, from configuring your firewall and how you load users to denying access and fetching the User object. Depending on what you need, sometimes the initial setup can be tough. But once it\'s done, Symfony\'s security system is both flexible and (hopefully) fun to work with.','2017-02-23 10:00:00','2017-02-20 20:46:33',1),
	(6,'Task 2','Security','If, however, a user is retrieved and returned, the checkCredentials() method kicks in. Quite expectedly, the purpose of this method is to verify that the passed credentials match those of the user that was found. And the same rules apply, if we return NULL or throw an exception, we fail the authentication.','2017-02-22 08:00:00','2017-02-20 20:49:21',1),
	(7,'Task 3','Forms','Creating a Simple Form\r\nSuppose you\'re building a simple todo list application that will need to display \"tasks\". Because your users will need to edit and create tasks, you\'re going to need to build a form. But before you begin, first focus on the generic Task class that represents and stores the data for a single task:','2017-02-22 11:00:00','2017-02-20 20:54:15',1),
	(8,'Task 4','Console','Become familiar with console commands\r\nThe Symfony framework provides lots of commands through the bin/console script (e.g. the well-known bin/console cache:clear command). These commands are created with the Console component. You can also use it to create your own commands.','2017-02-23 09:00:00','2017-02-20 20:57:06',0),
	(12,'Task Completed and Archived','Programming','Make To Do list Application','2017-02-21 15:00:00','2017-02-21 00:57:40',2);

/*!40000 ALTER TABLE `to_do` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `email`, `name`, `role`, `password`)
VALUES
	(7,'user@example.com','user','ROLE_USER','$2y$13$CiEJPvUYn6n.9a.lQ4G6ROMfDTmITanLPSpQ/eJ6DYSyBs.w1hJGS');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
