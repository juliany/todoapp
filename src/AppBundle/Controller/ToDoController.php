<?php

namespace AppBundle\Controller;

use AppBundle\Form\ToDoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\ToDo as ToDo;

/**
 * @Security("has_role('ROLE_USER')")
 */

class ToDoController extends Controller
{

    /**
     * @Route("/todos/create", name="todo_create")
     */
    public function createAction(Request $request)
    {
        $now = new\DateTime('now');
        $todo = new ToDo();
        $todo->setCreateDate($now);

        $form = $this->createForm(ToDoType::class,$todo);
        $form->handleRequest($request);

        if($form->isSubmitted() &&  $form->isValid()){

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager -> persist($todo);
            $entityManager -> flush();

            $this->addFlash(
                'notice',
                'Todo Added'
            );
            return $this->redirectToRoute('todo_list');
        }

        return $this->render('todos/create.html.twig', array(
            'form' => $form->createView()

        ));
    }

    /**
     * @Route("/todos/details/{id}",  name="todo_details")
     */
    public function detailsAction($id)
    {
        $todo = $this->getDoctrine()
            ->getRepository('AppBundle:ToDo')
            ->find($id);

        return $this->render('todos/details.html.twig', array(
            'todo'=>$todo
        ));

    }

    /**
     * @Route("/todos/edit/{id}", name="todo_edit")
     */
    public function editAction(Request $request, $id)
    {
        $todo = $this->getDoctrine()
            ->getRepository('AppBundle:ToDo')
            ->find($id);

        $form = $this->createForm(ToDoType::class,$todo);

        $form->handleRequest($request);
        if($form->isSubmitted() &&  $form->isValid()){

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager -> persist($todo);
            $entityManager -> flush();

            $this->addFlash(
                'notice',
                'Todo Edited'
            );
            return $this->redirectToRoute('todo_list');

        }

        return $this->render('todos/edit.html.twig', array(
            'todo' => $todo,
            'form' => $form->createView()

        ));

    }


    /**
     * @Route("/todos/archived", name="todo_archive")
     */

    public function archiveAction(Request $request) {
        $todos = $this->getDoctrine()
            ->getRepository('AppBundle:ToDo')
            ->findByStatus(2);



        return $this->render('todos/index.html.twig', array(
            'todos' => $todos
        ));
    }


}

