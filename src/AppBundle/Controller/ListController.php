<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class ListController extends Controller
{

    /**
     * @Route("/", name="todo_list")
     */

    public function listAction(Request $request) {
        $todos = $this->getDoctrine()
            ->getRepository('AppBundle:ToDo')
            ->findByStatus([0,1]);


        return $this->render('todos/index.html.twig', array(
            'todos' => $todos
        ));
    }

}
