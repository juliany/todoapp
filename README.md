Symfony ToDo Application
========================

The "Symfony ToDo Application" is a application created for pleasure.

Requirements
------------

  * PHP 5.5 or higher;
  * MySQL 5.5.5-10.1.17-MariaDB or higher
  * PDO-SQLite PHP extension enabled;
  * Symfony 2.8 long-term support

Installation
------------

    $ git clone https://juliany@bitbucket.org/juliany/todoapp.git
    $ cd todoapp/
	$ curl -sS https://getcomposer.org/installer | php
    $ php composer.phar install

Database
-------

Database sql files are located at todoapp/sql folder. 


Usage
-----

If you have PHP 5.5 or higher, there is no need to configure a virtual host
in your web server to access the application. Just use the built-in web server:

```bash
$ cd todoapp/
$ php app/console server:run
```

This command will start a web server for the Symfony application. Now you can
access the application in your browser at <http://localhost:8000>. You can
stop the built-in web server by pressing `Ctrl + C` while you're in the
terminal.

ToDo Application user credentials

user: user@example.com
pass: password